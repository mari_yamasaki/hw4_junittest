package homework4;

import java.util.*;

public class Rotate {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while(sc.hasNext()){
			String str = sc.next();
		int n = sc.nextInt();
		System.out.println(rotate(str, n));
		}
	}

	public static  char[] rotate(String str, int n) {
		char[] ans = new char[str.length()];
		int x = n % str.length();
		int current = str.length() - x;
		for (int i = 0; i < str.length(); i++) {	
	    int p = current%str.length();
			ans[i] = str.charAt(p);
			current++;
		}
		return ans;
	}
}
