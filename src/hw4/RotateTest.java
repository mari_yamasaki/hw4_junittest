package hw4;

import static org.junit.Assert.*;
import homework4.Rotate;
import org.junit.Test;

public class RotateTest {

	@Test
	public void testRotate() {
		String str = "abcde";
		String exp1 = "deabc"; 
		String exp2 = "bcdea";
		String exp3 = "abcde";
		String exp4 = "cdeab";
		String exp5 = "eabcd";
		String exp6 = "deabc";
		
	    String act1 = new String(Rotate.rotate(str, 2));
		String act2 = new String(Rotate.rotate(str, -1));
		String act3 = new String(Rotate.rotate(str, 0));
		String act4 = new String(Rotate.rotate(str, 8));
		String act5 = new String(Rotate.rotate(str, -9));
		String act6 = new String(Rotate.rotate(str, 17));
		
		assertEquals(act1,exp1);
		assertEquals(act2,exp2);	
		assertEquals(act3,exp3);	
		assertEquals(act4,exp4);	
		assertEquals(act5,exp5);	
		assertEquals(act6,exp6);			
	}
}
